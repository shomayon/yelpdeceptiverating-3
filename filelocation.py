# Absolute location of data files
SAMPLE_USER_FILE = './unitest/sample_user.csv'
SAMPLE_REVIEW_FILE = './unitest/sample_review.csv'
SMALLER_REVIEW = './unitest/smaller_review.csv'
USER_FILE = '/Users/viettrinh/Desktop/UCSC/CS_242/Project/Dataset/yelp_dataset_challenge_academic_dataset/CSV/yelp_academic_dataset_user.csv'
REVIEW_FILE = '/Users/viettrinh/Desktop/UCSC/CS_242/Project/Dataset/yelp_dataset_challenge_academic_dataset/CSV/yelp_academic_dataset_review.csv'


# Relative location of generated output files
GEN_SAMPLE_POP_UID = './gen/sample_pop_uid.txt'
GEN_POP_UID = './gen/gen_pop_uid.txt'
