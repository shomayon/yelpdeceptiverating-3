#!/usr/bin/python3
"""
This module runs spam score on a subset of the dataset
"""
__author__ = "Vikrant More" # hit me up - vmore@ucsc.edu

from spam_score import *
import filelocation
import numpy as np

review_csv_file = filelocation.SMALLER_REVIEW
num_rows = 100

reviews = spam_score.pull_data_csv(review_csv_file, num_rows)

business_id, business_review_sum, business_review_count, review_id = spam_score.generic_data(reviews)

RD, EXT, ETF, ISR, EXC, PP1, PC, RL, business_id = spam_score.extract_review_features(reviews, business_id, business_review_sum, business_review_count)

MNR, PR, NR, avgRD, WRD, BST, ERD, ETG = spam_score.extract_business_features(reviews, RD, business_id, business_review_count)

#review_feature_vector
review_feature_poles = [1, 1, 1, 1, 1, 0, 1]
# spam_score.compute_spam_score(review_featurePoles, X_review)
