#!/usr/bin/python -tt
# This module is the unit test of Viet Trinh for popular
__author__ = "Viet Trinh"  # vqtrinh@ucsc.edu

from popular import *
import filelocation


def user_test():
    return user.get_popular_users(filelocation.SAMPLE_USER_FILE)


def business_test():
    bus_list, bus_trusty = business.get_business(filelocation.GEN_SAMPLE_POP_UID, filelocation.SAMPLE_REVIEW_FILE)
    print bus_list
    print bus_trusty
