#!/usr/bin/python3
# coding=utf-8
"""
This module extracts a whole bunch of features
Then computes a spam score
"""
__author__ = "Vikrant More" # hit me up - vmore@ucsc.edu

import numpy as np
import pandas
import re
import nltk
import csv
from collections import Counter
from nltk.corpus import stopwords
import datetime
# from nltk.corpus import subjective
# from nltk.stem import WordNetLemmatizer

def pull_data_csv(review_csv_file, num_rows):
    """
    Pulls data from the csv file
    """
    columns = ["nothing", "user_id", "review_id", "review_text", "votes_cool", "business_id", "votes_fun", "stars", "date", "type", "votes_useful"]

    reviews = pandas.io.parsers.read_csv(review_csv_file, names=columns, skiprows = 1, parse_dates=['date'], nrows = num_rows)

    return reviews

def generic_data(reviews):
    #unique business ids
    business_id = set(reviews.business_id.tolist())

    #dictionary of business id vs sum of all reviews of a business
    business_review_sum = dict(reviews.groupby('business_id')['stars'].sum())

    #dictionary of business id vs number of all reviews of a business
    business_review_count = dict(reviews.groupby('business_id')['stars'].count())

    #list of all reviews
    review_id = reviews.review_id.tolist()

    return business_id, business_review_sum, business_review_count, review_id

def feature_EXT(reviews):
    """
    Extremity of a review: If rating is 1 or 5 give a value of 1, for all other give 0
    """
    EXT = dict()
    for index, row in reviews.iterrows():
        if row["stars"] == 1 or row["stars"] == 5:
            EXT[row["review_id"]] = 1
        else:
            EXT[row["review_id"]] = 0

    return EXT

def feature_RD(reviews, business_review_sum, business_review_count):
    """
    Absolute rating deviation from a business's average rating
    """

    #dictionary of average review value for each business
    avg_reviews_business = {id: business_review_sum[id]/business_review_count[id] for id in business_review_count}

    RD = dict()
    for index, row in reviews.iterrows():
        RD[row["review_id"]] = abs(row["stars"] - avg_reviews_business[row["business_id"]])

    return RD

def feature_ISR(reviews):
    """
    Is singleton? If review is user's sole review, then ISR = 1, otherwise 0
    """

    #dictionary of number of reviews per user
    user_review_count = dict(reviews.groupby('user_id')['review_id'].count())

    ISR = dict()
    for index, row in reviews.iterrows():
        if user_review_count[row["user_id"]] == 1:
            ISR[row["review_id"]] = 1
        else:
            ISR[row["review_id"]] = 0

    return ISR


def feature_ETF(reviews):
    """
    Early time frame. spammers often review early to increase impact
    """
    #dictionary of earliest review date of a business
    first_review_date = dict(reviews.groupby('business_id')['date'].min())

    delta = pandas.Timedelta('210 days') # 7 months. paper says so
    beta = 0.69 # paper says so

    ETF = dict()
    for index, row in reviews.iterrows():
        f = 0
        if (row["date"] - first_review_date[row["business_id"]]) < pandas.Timedelta(210, unit = 'd'):
            f = (row["date"] - first_review_date[row["business_id"]]) / delta
        if f > beta:
            ETF[row["review_id"]] = 1
        else:
            ETF[row["review_id"]] = 0

    return ETF

def feature_EXC_PP1_PC_RL(reviews, business_id, business_review_count):
    EXC = dict() # number of exclamations
    PP1 = dict() # ratio of I, me pronouns
    PC = dict() # ratio of all capital words
    RL = dict() # Average review length for a business

    review_length_sum_business = dict.fromkeys(business_id, 0)

    # splitter = re.compile("\\W*")
    splitter = re.compile("(\W)")
    english_stops = set(stopwords.words('english'))

    for index, row in reviews.iterrows():

        review_text = row["review_text"]
        l = [word for word in splitter.split(review_text)]
        # print(l)
        review_length_sum_business[row["business_id"]] += len(l)

        count_pc, count_pp, count_res = 0, 0, 0
        for word in l:
            # print(word)
            if word.isupper(): count_pc += 1
            if word in english_stops: count_pp += 1
            if word == "!":
                count_res += 1
        PC[row["review_id"]] = count_pc/len(l)
        PP1[row["review_id"]] = count_pp/len(l)
        EXC[row["review_id"]] = count_res/len(l)

    RL = {id: review_length_sum_business[id]/business_review_count[id] for id in business_id}

    return EXC, PP1, PC, RL

def feature_PR_NR(reviews, business_id, business_review_count):
    """
    PR, NR - ratio of positive reviews, ratio of negative reviews
    """
    NR = dict()
    PR = dict()

    reviews_3plus = reviews[reviews['stars'] > 3].groupby('business_id')['stars'].count()
    reviews_3minus = reviews[reviews['stars'] < 3].groupby('business_id')['stars'].count()

    for business in business_id:
        if business in reviews_3minus:
            NR[business] = reviews_3minus[business] / business_review_count[business]
        else:
            NR[business] = 0
        if business in reviews_3plus:
            PR[business] = reviews_3plus[business] / business_review_count[business]
        else:
            PR[business] = 0

    return PR, NR

def feature_MNR(reviews):
    """
    MNR - normalized maximum number of reviews written in a day
    """
    MNR = dict()

    for business, datelist in reviews.groupby('business_id')['date'].apply(list).iteritems():
        mc, n_mc = Counter(datelist).most_common(1)[0]
        MNR[business] = n_mc

    norm_max = 1 / max(MNR.values())
    for k, v in MNR.items(): MNR[k] *= norm_max

    return MNR

def feature_avgRD(reviews, RD):
    """
    Average rating deviation for a business
    """
    avgRD = dict()

    for business, review_list in reviews.groupby('business_id')['review_id'].apply(list).iteritems():
        sum = 0
        for i in range(len(review_list)): sum += RD[review_list[i]]
        avgRD[business] = sum/len(review_list)

    return avgRD

def feature_ERD(reviews):
    """
    Entropy of rating distribution of business's ratings
    """
    ERD = dict()
    for business, rating_list in reviews.groupby('business_id')['stars'].apply(list).iteritems():
        rating_counter = dict(Counter(rating_list))
        length_rating = 1 / len(rating_list)
        if bool(rating_counter) is True:
            for k, v in rating_counter.items(): rating_counter[k] *= length_rating
            sum = 0
            for k, v in rating_counter.items():
                if v != 0: sum += v * np.log2(v)
            ERD[business] = sum

    return ERD

def feature_BST(reviews):
    """
    Burstiness of a reviewer. If diff between first and last review is > 28 days then BST = 0, else (1 - diff/tau)
    """
    BST = dict()
    diff_user_review_date = reviews.groupby('user_id')['date'].max() - reviews.groupby('user_id')['date'].min()
    tau = pandas.Timedelta('28 days')

    for user, diff in diff_user_review_date.iteritems():
        if diff > tau: BST[user] = 0
        else: BST[user] = 1 - (diff / tau)

    return BST



def feature_WRD(reviews, RD):
    """
    Weighted rating deviation, where reviews are weighed by recency.
    weight is (1/t^α) where t is the rank order(temporal) of a review for the business.
    α is the decay rate = 1.5
    """
    WRD = dict()
    W = dict()
    business_unique_review_dates = dict()
    alpha = 1.5
    for business, date_list in reviews.groupby('business_id')['date'].apply(list).iteritems():
        date_counter = dict(Counter(date_list))
        business_unique_review_dates[business] = date_counter

    for index, row in reviews.iterrows():
        if row["business_id"] in business_unique_review_dates:
            if bool(business_unique_review_dates[row["business_id"]]) is True:
                for unique_date, count in business_unique_review_dates[row["business_id"]].items():
                    rank = 1
                    if row["date"] == unique_date:
                        rank = count
                    else:
                        rank += count
                W[row["review_id"]] = 1 / (rank**alpha)

    for business, review_list in reviews.groupby('business_id')['review_id'].apply(list).iteritems():
        sum_w, sum_dev_w = 0, 0
        for review in review_list:
            sum_w += W[review]
            sum_dev_w += RD[review] * W[review]
        WRD[business] = sum_dev_w / sum_w

    return WRD

def feature_ETG(reviews):
    """
    Entropy of temporal gaps  deltaT. Given the temporal line-up of a
    business's reviews, each  deltaT denotes the temporal gap in days between consecutive pairs
    """
    ETG = dict()

    time_gaps = [0 , 24, 3*24, 5*24, 9*24, 17*24]
    for business, date_list in reviews.groupby('business_id')['date'].apply(list).iteritems():
        for i in range(len(date_list)):
            date_list[i] = (date_list[i]-datetime.datetime(1970,1,1)).total_seconds()/3600
        # print(business, date_list)
        gap = []
        for i in range(len(date_list)):
            if i + 1 <= len(date_list) - 1:
                gap.append(date_list[i+1] - date_list[i])
        binned = np.histogram(gap, time_gaps)[0]

        if binned.sum() != 0:
            binned = binned / binned.sum()

        sum_p = 0
        for p in np.nditer(binned):
            if p != 0: sum_p += -p*np.log2(p)
        # print(binned)
        ETG[business] = sum_p

    return ETG

def compute_spam_score(featurePoles, X):
    """
    compute spam score
    input X is a NxD matrix, numpy array
    input featurePoles is a D dimensional array with 1 for high and 0 for low
    """
    featurePoles = []
    num_data_pts = X.shape[0]
    num_features = X.shape[1]
    comp = np.ones(num_data_pts, num_features)

    for j in range(num_features):

        feature_set = X[:,j]
        sorted_feature_set = np.sort(feature_set)

        for i in num_data_pts:

            if(featurePoles[j] == 1):#When a high value for the feature is suspicious
                idx = np.searchsorted(sorted_feature_set, feature_set[i], side = 'left')
                NCDF = idx / num_data_pts
                comp[i,j] = 1 - NCDF
            else:#When low value for a feature is suspicious
                idx = np.searchsorted(sorted_feature_set, feature_set[i], side = 'right')
                NCF = idx / num_data_pts
                comp[i,j] = NCDF

    spam_score = 1 - np.sqrt(np.sum(comp*comp, axis=1)/num_features)

    return spam_score

def extract_review_features(reviews, business_id, business_review_sum, business_review_count):
    RD = feature_RD(reviews, business_review_sum, business_review_count)
    EXT = feature_EXT(reviews)
    ETF = feature_ETF(reviews)
    ISR = feature_ISR(reviews)
    EXC, PP1, PC, RL = feature_EXC_PP1_PC_RL(reviews, business_id, business_review_count)

    return RD, EXT, ETF, ISR, EXC, PP1, PC, RL, business_id

def extract_business_features(reviews, RD, business_id, business_review_count):
    MNR = feature_MNR(reviews)
    PR, NR = feature_PR_NR(reviews, business_id, business_review_count)
    avgRD = feature_avgRD(reviews, RD)
    WRD = feature_WRD(reviews, RD)
    BST = feature_BST(reviews)
    ERD = feature_ERD(reviews)
    ETG = feature_ETG(reviews)

    return MNR, PR, NR, avgRD, WRD, BST, ERD, ETG
